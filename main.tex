\documentclass[ngerman, 12pt]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{babel}
\usepackage{cleveref}
\usepackage{csquotes}
\usepackage{graphicx}
\usepackage{microtype}
\usepackage{siunitx}
    \DeclareSIUnit{\feet}{ft}
\usepackage{libertine}
\usepackage{circuitikz}
\usetikzlibrary{calc}
\tikzset{
	every node/.style={
		font=\small\sffamily,
	}
}
\usepackage[
	backend=bibtex, 
	style=ieee,
	doi=false,
	isbn=false,
	url=false,
	eprint=false
]{biblatex}
\addbibresource{references.bib}

\usepackage{pdfpages}

\title{Erstellung eines wissenschaftlichen Posters}
\author{
    P\&S Erneuerbare Energien\\
    und Netto-Null-Emissions-Ziel in der Schweiz\\
    ETH Zürich
}
\date\today

\begin{document}

    \maketitle

    \begin{abstract}
        Im Rahmen dieses P\&S-Kurses soll ein Poster erstellt werden, welches
        das von Ihnen durchgeführte Projekt einem breiten Publikum
        wissenschaftlich zusammenfasst.
        Das vorliegende Dokument bietet eine kurze Einleitung ins Konzept des
        Posters, erklärt welche Inhalts-, Gliederungs- und Formatkriterien zu
        berücksichtigen sind, und gibt schliesslich praktische Tipps.
	\end{abstract}

	\section{Das Konzept des Posters}

    Ein Poster ist ein Medium, das in den Wissenschaften, vor allem auf
    Konferenzen und Tagungen, der Vermittlung von Forschungsideen und
    -ergebnissen dient.
    Ein beispielhaftes Layout eines Posters zeigt \cref{figure:layout}.
    Poster werden normalerweise gross ausgedruckt und als visuelle Hilfe
    von einer vortragenden Person benutzt, können jedoch auch projiziert oder
    als selbstständiges Medium gedacht werden.
    Im Gegensatz zu einem wissenschaftlichen Artikel versucht das Poster nicht,
    jede Einzelheit einer Forschungsarbeit zu beschreiben, sondern es
    konzentriert sich auf ihre wesentlichen Informationen.  
    Ausgeführt wird die Forschung oft in einem parallel eingereichten Artikel
    und jedenfalls durch den mündlichen Austausch.

	\begin{figure}[t!]
		\centering
		\input{layout.tex}
        \caption{Beispielhaftes Layout eines wissenschaftlichen Posters.}
		\label{figure:layout}
	\end{figure}

    Ein Poster ist \emph{nicht} einfach eine abgekürzte und gross ausgedruckte
    Version eines Artikels. 
    Inhalt, Gliederung und Format müssen angepasst werden, damit die
    wichtigsten Punkte der Forschung innerhalb von wenigen Minuten und selbst
    in nicht idealen Räumen ---überfüllt, laut und schlecht beleuchtet---
    verständlich sind.
    Ein Poster ist auch \emph{kein} Beweis, wie viel Arbeit geleistet wurde.

    \section{Erste Informationen}

    Eine der ersten Entscheidungen, die man über das Poster trifft, ist seine
    Sprache.
    Auf vielen Konferenzen ist die Kommunikationssprache Englisch, da diese
    Sprache ein breiteres Publikum erreicht. 
    Die Sprache wird aus diesem Grund meistens von der Organisation vorgegeben.
    Der Einheitlichkeit halber wird aber in diesem Dokument davon ausgegangen,
    dass das Poster auf Deutsch gestaltet wird.
    Ob im P\&S-Kurs Englisch oder Deutsch verwendet wird, ist Geschmackssache.
	
    Die nächste Entscheidung betrifft die wohl wichtigste Information des
    Posters: seinen Titel.	
    Dieser soll den Inhalt kurz und präzise beschreiben sowie das Interesse
    wecken.
    Es ist keine einfache Frage, ob und wie man diese beiden Ziele erreichen
    kann.
    Man nehme als Beispiel folgenden Titel:
	\begin{displayquote}
        1.1 Einfluss der Rotordrehzahl eines Windgenerators auf das
        Leistungskennfelds des Rotors und den maximalen Rotorleistungsbeiwert
	\end{displayquote}
    Einerseits lässt sich sehr genau schliessen, worum es im Poster geht, aber
    andererseits kann die Hauptbotschaft durch die zu spezialisierten
    Fachbegriffe verloren gehen.
    (Es ist auch nicht klar, wie man einen so langen Titel in einer grossen
    Schrift ausdrucken kann, ohne dass er wertvollen Platz wegnimmt.)
    Als Alternative bietet sich
	\begin{displayquote}
        1.2 Optimierung der Rotordrehzahl von Windgeneratoren	
	\end{displayquote}
    an, die bei manchen Zuschauerschaften die bessere Variante sein könnte,
    aber auch gedrängt klingt.
    Ein weiteres Beispiel ist der Titel
	\begin{displayquote}
        2.1 Abhängigkeit des Wirkungsgrades einer Solaranlage von der
        Sonnenhöhe und des Sonnenazimut
	\end{displayquote}
    Dieser lässt sich bestreitbar ohne hohen Bedeutungsverlust durch die
    folgende Variante ersetzen:
	\begin{displayquote}
        2.2 Warum richten sich die Solaranlagen in der Schweiz nach Süden?
	\end{displayquote}
    Grundsätzlich gilt: der Titel sollte auf Länge und unnötigen Jargon
    verzichten.
    Obwohl die Titel der in diesem P\&S-Kurs durchgeführten Projekte schon im
    Projektplan festgehalten wurden, spricht nichts dagegen, sie zu verbessern.

    Nach dem Titel kommen die Autor*innen. 
    Während es im wissenschaftlichen Umfeld ungeschriebene Regeln gibt, welche
    die Reihenfolge der Autor*innen bestimmen, ist diese Reihenfolge in diesem
    P\&S-Kurs beliebig. 
    Wichtig ist, dass jede beitragende Person aufgelistet wird.
    Sollte ein kleiner Beitrag von Dritten erwähnenswert sein, so kann diesen
    Personen in einem separaten Abschnitt oder einer Fussnote gedankt werden.
    Neben den Namen der Autor*innen müssen auch ihre Zugehörigkeiten
    ---sogenannte Affiliationen--- sowie zumindest eine Kontaktadresse stehen.
    Dies erleichtert die Kontaktaufnahme nach der Posterpräsentation.

    Bei der Erstellung eines Posters stellt sich die Frage, ob dem Titel und
    den Autor*innen eine Zusammenfassung folgt. 
    Diese kann entbehrt werden, da sie in der Regel von der Organisation im
    Voraus bekannt gemacht wird und jedenfalls im eingereichten Artikel
    enthalten ist.
    Ein weiterer Grund, die Zusammenfassung auszulassen, ist die Kürze eines
    Posters.	

    \section{Inhalt und Gliederung}
    
    Auch wegen der Kürze eines Posters empfehlen einige
    Autorinnen~\cite{briscoe_preparing_1996}, sich beim Inhalt auf eine
    Handvoll Kernideen zu beschränken.
    In welcher genauen Form diese Ideen am besten dargestellt werden, also ob
    mit Text oder Abbildungen, kommt auf die Funktion des Posters an.
    Ist es nur eine visuelle Hilfe, die nicht über die Präsentation hinaus
    leben wird, so kann man sich mehr Freiheiten erlauben, wie z.B. einen Fokus
    auf Abbildungen mit nur einigen Stichwörtern.
    Hat man aber vor, das Poster lange nach der Präsentation hängen zu lassen,
    so könnten sich mehrere vollständige Sätze zur Lesbarkeit lohnen.

    Die Gliederung entspricht normalerweise dem sogenannten
    \textsc{imrad}-Schema~\cite{gastel2016}, das für die folgenden Teile steht:
	\begin{center}
        \begin{tabular}{cll}
            I & Introduction & Einleitung \\
            M & Methodology & Experimente \\
            R & Results & Resultate \\
            A & Analysis & Analyse \\
            D & Discussion and conclusion & Fazit \\
		\end{tabular}
	\end{center}
    Dieses Schema ist überall in der wissenschaftlichen Kommunikation, von
    Artikeln über Berichten bis zu Präsentationen, zu finden und eignet sich
    auch für Poster.
    Je nach Art der Forschungsarbeit sind Abweichungen sinnvoll.
    Im Fachgebiet Elektrotechnik wird z.B. der Abschnitt «Experimente» oft
    durch andere Abschnitte ersetzt, welche die Modelierungsannahmen und die
    mathematische Formulierung darstellen.
    Natürlich soll dieses \textsc{imrad}-Schema bei Quellenangaben mit einem
    kurzen Literaturverzeichnis ergänzt werden.

    \section{Format}
	
    Für das Format, also für die visuelle Gestaltung eines Posters, gibt es auf
    Konferenzen normalerweise keine Vorschriften.
    Die einzige Ausnahme sind die maximale Breite bzw. Höhe, die z.B. in
    Nordamerika typischerweise \SI{4}{\feet}\,$\times$\,\SI{4}{\feet} betragen.
    Obwohl diese maximale Grösse quadratisch ist, können andere
    Seitenverhältnisse bzw. -ausrichtungen praktischer sein.
    Das Querformat etwa hat den Vorteil, dass der Grossteil des Inhalts auf
    Augenhöhe und für mehrere Personen, die nebeneinander stehen, sichtbar
    bleibt.
    Das Hochformat für seinen Teil braucht weniger Platz und eignet sich so
    dafür, dass man das Poster längere Zeit an einer Wand hängen lässt;
    vielleicht deswegen haben die meisten Poster, die in den ETZ- und
    ETL-Gebäuden der ETH~Zürich zu sehen sind, ein Hochformat.

    Auch die Auswahl von Farbe und Schriftart bietet Spielraum.
    Damit das Poster aber nicht wegen dieser Elemente, sondern wegen des
    Inhalts und des guten Designs auffällt, ist es empfehlenswert, sich auf
    eine Farbpalette und eine einzige Schriftart zu beschränken.
    Viele Universitäten stellen Richtlinien visueller Identität zur Verfügung.
    Diese einzuhalten, trägt dazu bei, dass die vorher erwähnten Elemente sowie
    Universitäts- und Institutslogos miteinander harmonieren.

    In diesem P\&S-Kurs sind sowohl das Hochformat als auch die Farbpalette der
    ETH~Zürich vorgeschrieben (siehe Vorlage auf der Moodle-Seite).

    Eine weitere Frage des Formats ist die Schriftgrösse.
    Die Schrift sollte gross genug sein, damit der gesamte Text trotz
    Entfernung und schlechter Beleuchtung lesbar ist.
    Man empfiehlt in~\cite{briscoe_preparing_1996} 36\,pt für den Titel und
    30\,pt für den restlichen Text.
    Andere akzeptablen Schriftgrössen lassen sich feststellen, indem man
    möglichst ohne Verkleinerung bzw. -grösserung das Poster auf einer Wand
    projiziert oder nur einen Abschnitt davon auf günstigem Papier ausdruckt.
    Sollte der Inhalt bei der festgestellten Schriftgrösse nicht im Poster
    passen, so empfiehlt es sich, diesen Inhalt zu reduzieren anstatt die
    Schrift zu verkleinern.
    Dies kann zwar zu einem mühsamen Prozess werden, führt aber zu besseren
    Resultaten.

	\begin{figure}[t!]
		\centering
		\input{zeichnung.tex}
        \caption{Beispiel einer schematischen Zeichnung. Diese stellt nur die
        relevanten Schaltungen eines Experiments dar und ist wegen Einfachheit,
        guten Kontrastes und hoher Auflösung gut lesbar.
        \textit{Nach~\cite{romer_what_1982}.}}
		\label{fig:circuit}
	\end{figure}

    Posterelemente, welche besondere Aufmerksamkeit verdienen, sind die
    Abbildungen.
    Zum Abschnitt «Experimente» gehören Darstellungen der Schaltungen sowie der
    räumlichen Anordnung von Geräten, wie z.B. der Abstand zwischen einem
    Ventilator und einem Windrad oder der Winkel, in dem ein Solarmodul
    bestrahlt wird.
    Obwohl diese Variablen oft durch Fotografien abgebildet werden, sind
    schematische Zeichnungen geeigneter (\cref{fig:circuit}).
    Da diese auf unnötige Details verzichten und mit beliebiger Auflösung
    ausgedruckt werden können, sind sie lesbarer und ästhetischer.
    Auch dargestellt wird in diesem Abschnitt bei komplexeren Experimenten ihr
    Verlauf, etwa in der Form eines Flussdiagrammes.

    Zum Abschnitt «Resultate» gehören meistens eher Graphiken.
    Auch wenn im Artikelformat Tabellen aufgrund der Natur der Daten sinnvoller
    wären, sind Graphiken zu bevorzugen (es sei denn, die Tabellen haben zu
    wenige Daten, als dass man sie graphisch darstellen könnte).
    Zur Erstellung von Graphiken verweise man hier auf den Vortrag «Darstellung
    von Messwerten», der auf der Moodle-Seite zugänglich ist.

    Schliesslich stellt sich die Frage, ob Bildunterschriften in einem Poster
    notwendig sind.
    Für sie spricht die Tatsache, dass Abbildungen somit zu selbstständigen
    Elementen werden und verständlich sind, auch wenn niemand da ist, um sie zu
    erklären.
    Dagegen sprechen der Gebrauch wertvollen Platzes und die Verkomplizierung
    des Posters.
    Dass Bildunterschriften bei Artikeln unentbehrlich sind, sagt natürlich
    nichts über ein Poster, denn bei Letzterem erfolgt der Verweis auf
    Abbildungen oft mündlich und deswegen bedarf man der in der
    Bildunterschrift enthaltenen Nummer nicht.
    Dasselbe gilt für Tabellenüberschriften.

    \section{Sonstiges}

    Zur tatsächlichen Erstellung des Posters sind die Programme PowerPoint,
    Illustrator und \LaTeX{} beliebt.
    PowerPoint hat den Vorteil, dass viele Nutzer*innen es schon beherrschen
    und die Lernkurve relativ einfach ist, während Illustrator ein
    professionelleres Resultat ermöglicht; für die beiden Programme stellt die
    ETH~Zürich leicht zu findende Vorlagen zur Verfügung.
    \LaTeX{} kann auch zu professionellen Ergebnissen führen, besonders in
    akademischen Kreisen und bei vielen mathematischen Formeln, aber die
    Benutzung ist nicht intuitiv und kann viel Zeit in Anspruch nehmen.
    Jedenfalls wird hier betont, dass nicht die Programme, sondern die
    richtigen Design-Entscheidungen ein gutes Poster ausmachen.

    Ausgedruckt wird das Poster, wie bereits erwähnt, im Grossformat.
    Manche Druckerein benötigen für dieses Format mehrere Tage, die vor einer
    Konferenz eingeplant werden müssen.
    Die Auswahl des Postermaterials kommt unter anderem auf den Transport an.
    Verfügt man über eine Zeichenrolle und darf man diese transportieren (was
    in vielen Fluggesellschaften der Fall ist), ist ein hochwertiges Papier
    ideal.  
    Auf diesem sind in der Regel Text und Abbildungen besser lesbar als auf
    anderen Materialien.
    Darf man aber nur einen Koffer transportieren, so eignet sich eher Stoff,
    der sich falten lässt.
    Weder Ausdruck noch Transport müssen in diesem P\&S-Kurs berücksichtigt
    werden, da sie Sache der Assistenten sind.

    Nicht obligatorische, doch hilfreiche Ergänzungen zum Poster sind
    Visitenkarten und Flugblätter. 
    Die Ersteren erleichtern ferner die Kontaktaufnahme nach der
    Posterpräsentation, während die Letzteren wichtige Punkte der
    Forschungsarbeit ausführen.
    In dem Falle, dass ein Artikel parallel eingereicht wurde, ist ein
    Flugblatt jedoch weniger sinnvoll.

    Weitere Hinweise sind in den folgenden Büchern zu finden.
    In~\cite{gastel2016} wird das \textsc{imrad}-Schema erläutert sowie
    sämtliche Aspekte der wissenschaftlichen Kommunikation.
    Edward Tufte präsentiert in~\cite{tufte_visual_2007} zahlreiche
    ausgezeichnete Abbildungen und führt seine bekannten Prinzipien ein; man
    denke etwa an die Minimierung von «Chartjunk», also von unnötigen Elementen
    von Graphiken.
    In~\cite{briscoe_preparing_1996} werden auch Abbildungen thematisiert, aber
    ein separates Kapitel ist den Posters gewidmet. 
    Aus diesem Kapitel stammen einige der hier vorgestellten Hinweise.
	
    Zum Schluss muss daran erinnert werden, dass ein Poster sehr von
    Kreativität profitiert.
    \emph{Es gibt keine festen Regeln.}
    Wie der typographische Designer Robert Bringhurst über seine Disziplin
    sagte, «by all means break the rules, and break them beautifully,
    deliberately, and well».

    \printbibliography

\end{document}
